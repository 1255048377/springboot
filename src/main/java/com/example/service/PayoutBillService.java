package com.example.service;


import com.example.dao.IncomeBillDao;
import com.example.dao.PayoutBillDao;
import com.example.entity.IncomeBill;
import com.example.entity.Parmas;
import com.example.entity.PayoutBill;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;


@Service
public class PayoutBillService {

    @Resource
    private PayoutBillDao payoutBillDao;


    public PageInfo<PayoutBill> findBySearch(Parmas params) {
        //开启分页查询
        PageHelper.startPage(params.getPageNum(), params.getPageSize());
        List<PayoutBill> list =payoutBillDao.findBySearch(params);
        return PageInfo.of(list);
    }

//    public void add(IncomeBill incomeBill) {
//        incomeBillDao.insertSelective(incomeBill);
//    }

    public void add(PayoutBill payoutBill) {
        if(payoutBill.getInoutclassify()== null)
            payoutBill.setInoutclassify("支出");
        payoutBillDao.insertSelective(payoutBill);
    }

    public void update(PayoutBill payoutBill) {
        payoutBillDao.updateByPrimaryKeySelective(payoutBill);
    }

    public void delete(Integer id) {
        payoutBillDao.deleteByPrimaryKey(id);
    }



    public List<PayoutBill> getPayoutBill() {
        return payoutBillDao.getPayoutBill();
    }


    public List<PayoutBill> findAll(Parmas params) {
        return payoutBillDao.findAll(params);
    }

    public List<PayoutBill> findAllinout(Parmas params) {
        return payoutBillDao.findAllinout(params);
    }
}