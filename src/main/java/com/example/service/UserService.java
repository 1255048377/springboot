package com.example.service;

import com.example.dao.UserDao;
import com.example.entity.Parmas;
import com.example.entity.User;
import com.example.exception.CustomException;
import com.example.exception.GlobalExceptionHandler;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.security.GeneralSecurityException;
import java.util.List;

@Service
public class UserService {

    @Resource
    private UserDao userDao;

    public List<User> getUser() {
        //return userDao.getUser();
        // 3. 使用引入的包
        return userDao.selectAll();
    }

    public PageInfo<User> findBySearch(Parmas params) {
        //开启分页查询
        PageHelper.startPage(params.getPageNum(), params.getPageSize());
        List<User> list = userDao.findBySearch(params);
        return PageInfo.of(list);
    }

    public void add(User user) {

        User user1= userDao.findByName(user.getName());
        //1.用户名一定要有，否则不让新增(后面需要用户名登录)
        if (user1 == null) {
            throw new CustomException("用户名不能为空");
        }
//        if(user1.getName()== null || user1.getName().isEmpty() ){
////            "".equals(user1.getName())
//            throw new CustomException("用户名不能为空");
//        }
        if(user1 != null){
            throw new CustomException("该用户名已存在，请勿重复添加");

        }
        //初始化一个密码
        if(user.getPassword()== null)
            user.setPassword("123456");
        userDao.insertSelective(user);
    }

    public void register(User user) {

        User user1= userDao.findByName(user.getName());
//        //1.用户名一定要有，否则不让新增(后面需要用户名登录)
//        if (user1 == null) {
//            throw new CustomException("用户名不能为空");
//        }
//        if(user1.getName()== null || user1.getName().isEmpty() ){
////            "".equals(user1.getName())
//            throw new CustomException("用户名不能为空");
//        }
        if(user1 != null){
            throw new CustomException("该用户名已存在，请勿重复添加");

        }
        //初始化一个密码
        if(user.getPassword()== null)
            user.setPassword("123456");
        userDao.insertSelective(user);
    }

    public void upadte(User user) {
        userDao.updateByPrimaryKeySelective(user);
    }

    public void delete(Integer id) {
        userDao.deleteByPrimaryKey(id);
    }

    public User login(User user) {

//        if (user == null) {
//            throw new CustomException("用户名不能为空");
//        }
//        if(user.getName()== null || user.getName().isEmpty() ){
////            "".equals(user1.getName())
//            throw new CustomException("用户名不能为空");
//        }
//        if(user.getPassword()== null || user.getPassword().isEmpty() ){
////            "".equals(user1.getName())
//            throw new CustomException("密码不能为空");
//        }
        User edituser = userDao.findByNameAndPassword(user.getName(),user.getPassword());
        if (edituser == null)
        {
            throw new CustomException("用户名或密码输入错误");
        }
        return edituser;
    }
}