package com.example.controller;


import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ObjectUtil;
import com.example.common.Result;
import com.example.entity.IncomeBill;
import com.example.entity.Parmas;
import com.example.entity.PayoutBill;
import com.example.service.IncomeBillService;
import com.example.service.PayoutBillService;
import com.github.pagehelper.PageInfo;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


@CrossOrigin
@RestController
@RequestMapping("/bill1")
public class PayoutBillController {

    @GetMapping
    public Result getPayoutBill() {
        List<PayoutBill> list=payoutBillService.getPayoutBill();
        return Result.success(list);
    }
    @Resource
    private PayoutBillService payoutBillService;

    @GetMapping("/search")
    public Result findBySearch(Parmas params) {
        PageInfo<PayoutBill> info = payoutBillService.findBySearch(params);
        return Result.success(info);
    }

    @PostMapping
    public Result save(@RequestBody PayoutBill payoutBill) {
        if(payoutBill.getId() == null) {
            payoutBillService.add(payoutBill);
        }else{
            payoutBillService.update(payoutBill);
        }
        return Result.success();
    }

    @DeleteMapping("/{id}")
    public Result delete(@PathVariable Integer id){
        payoutBillService.delete(id);
        return Result.success();
    }
    @GetMapping("/echarts/bie")
    public Result bie(@RequestParam String uname){
        Parmas params = new Parmas();
        params.setUname(uname);
        List<PayoutBill> list =  payoutBillService.findAll(params);
        Map<String,Long> collect=list.stream().filter(x -> ObjectUtil.isNotEmpty(x.getBookname())).collect(Collectors.groupingBy(PayoutBill::getBookname,Collectors.counting()));

//        List<Map<String,Object>> maplist = new ArrayList<>();
//        if (CollectionUtil.isNotEmpty(collect)) {
//            for(String key : collect.keySet())
//            {
//                Map<String, Object> map = new HashMap<>();
//                map.put("name", key);
//                map.put("value", collect.get(key));
//                maplist.add(map);
//            }
//        }
        // 使用流操作将账单按照账本名称分组，并对每组的账单金额求和
        Map<String, Double> groupedSum = list.stream()
                .collect(Collectors.groupingBy(PayoutBill::getBookname, Collectors.summingDouble(PayoutBill::getAccount)));

        // 将结果转换为需要的格式
        List<Map<String, Object>> maplist = new ArrayList<>();
        for (String key : groupedSum.keySet()) {
            Map<String, Object> map = new HashMap<>();
            map.put("name", key);
            map.put("value", groupedSum.get(key));
            maplist.add(map);
        }
        return  Result.success(maplist);
    }


    @GetMapping("/echarts/bar")
    public Result bar(@RequestParam String uname){
        Parmas params = new Parmas();
        params.setUname(uname);
        List<PayoutBill> list =  payoutBillService.findAllinout(params);
        Map<String,Long> collect=list.stream().filter(x -> ObjectUtil.isNotEmpty(x.getBookname())).collect(Collectors.groupingBy(PayoutBill::getBookname,Collectors.counting()));
        // 使用流处理数据，分别计算支出和收入
        Map<String, Double> payoutMap = list.stream()
                .filter(bill -> "支出".equals(bill.getInoutclassify()))
                .collect(Collectors.groupingBy(PayoutBill::getBookname, Collectors.summingDouble(PayoutBill::getAccount)));

        Map<String, Double> incomeMap = list.stream()
                .filter(bill -> "收入".equals(bill.getInoutclassify()))
                .collect(Collectors.groupingBy(PayoutBill::getBookname, Collectors.summingDouble(PayoutBill::getAccount)));

        // 提取账本名称作为 x 轴数据
        List<String> xAxis = new ArrayList<>(payoutMap.keySet());

        // 提取总支出作为 y 轴数据
        List<Double> yAxis = new ArrayList<>(payoutMap.values());

        // 提取总收入作为 y 轴数据
        List<Double> yAxis1 = new ArrayList<>();
        for (String bookname : xAxis) {
            yAxis1.add(incomeMap.getOrDefault(bookname, 0.0));
        }

        // 将数据封装成一个对象，返回给前端
        Map<String, Object> map = new HashMap<>();
        map.put("xAxis", xAxis);
        map.put("yAxis", yAxis);
        map.put("yAxis1", yAxis1);

//        List<String> xAxis = new ArrayList<>();
//        List<String> yAxis = new ArrayList<>();//支出数据
//        List<String> yAxis1 = new ArrayList<>();//收入数据
//
//        if(CollectionUtil.isNotEmpty(collect)){
//            for(String key:groupedSum.keySet())
//            {
//                xAxis.add(key);
//            }
//        }

        return  Result.success(map);
    }

    @GetMapping("/echarts/billclassifybie")
    public Result billclassifybie(@RequestParam String uname){
        Parmas params = new Parmas();
        params.setUname(uname);
        List<PayoutBill> list =  payoutBillService.findAll(params);
        Map<String,Long> collect=list.stream().filter(x -> ObjectUtil.isNotEmpty(x.getBookname())).collect(Collectors.groupingBy(PayoutBill::getBookname,Collectors.counting()));

// 使用流处理数据，根据 billclassify 分组，并对 account 求和
        Map<String, Double> groupedSum = list.stream()
                .collect(Collectors.groupingBy(PayoutBill::getBillclassify, Collectors.summingDouble(PayoutBill::getAccount)));
        // 将结果转换为需要的格式
        List<Map<String, Object>> maplist = new ArrayList<>();
        for (String key : groupedSum.keySet()) {
            Map<String, Object> map = new HashMap<>();
            map.put("name", key);
            map.put("value", groupedSum.get(key));
            maplist.add(map);
        }
        return  Result.success(maplist);
    }



    @GetMapping("/echarts/billclassifybar")
    public Result billclassifybar(@RequestParam String uname){
        Parmas params = new Parmas();
        params.setUname(uname);
        List<PayoutBill> list =  payoutBillService.findAll(params);
        Map<String,Long> collect=list.stream().filter(x -> ObjectUtil.isNotEmpty(x.getBookname())).collect(Collectors.groupingBy(PayoutBill::getBookname,Collectors.counting()));
        // 使用流处理数据，分别计算支出和收入
        Map<String, Double> payoutMap = list.stream()
                .filter(bill -> "支出".equals(bill.getInoutclassify()))
                .collect(Collectors.groupingBy(PayoutBill::getBillclassify, Collectors.summingDouble(PayoutBill::getAccount)));

        // 提取账本名称作为 x 轴数据
        List<String> xAxis = new ArrayList<>(payoutMap.keySet());
        // 提取总支出作为 y 轴数据
        List<Double> yAxis = new ArrayList<>(payoutMap.values());
        // 将数据封装成一个对象，返回给前端
        Map<String, Object> map = new HashMap<>();
        map.put("xAxis", xAxis);
        map.put("yAxis", yAxis);


        return  Result.success(map);
    }
}