package com.example.entity;

import javax.persistence.*;

@Table(name = "bill")
public class IncomeBill {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "bookname")
    private String bookname;
    @Column(name = "uname")
    private String uname;
    @Column(name = "sum")
    private String sum;
    @Column(name = "inoutclassify")
    private String inoutclassify;
    @Column(name = "billclassify")
    private String billclassify;
    @Column(name = "account")
    private Double account;
    @Column(name = "notes")
    private String notes;

    public String getUname() {
        return uname;
    }

    public void setUname(String uname) {
        this.uname = uname;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getSum() {
        return sum;
    }

    public void setSum(String sum) {
        this.sum = sum;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getBookname() {
        return bookname;
    }

    public void setBookname(String bookname) {
        this.bookname = bookname;
    }

    public String getInoutclassify() {
        return inoutclassify;
    }

    public void setInoutclassify(String inoutclassify) {
        this.inoutclassify = inoutclassify;
    }

    public String getBillclassify() {
        return billclassify;
    }

    public void setBillclassify(String billclassify) {
        this.billclassify = billclassify;
    }

    public Double getAccount() {
        return account;
    }

    public void setAccount(Double account) {
        this.account = account;
    }
}
