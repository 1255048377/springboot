package com.example.dao;

import com.example.entity.IncomeBill;
import com.example.entity.Parmas;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;


@Repository
public interface IncomeBillDao extends Mapper<IncomeBill> {


    @Select("select * from bill where inoutclassify = '收入' and bookname like concat('%', #{params.bookname}, '%') and billclassify like concat('%', #{params.billclassify}, '%') and uname =#{params.uname} ")
    List<IncomeBill> findBySearch(@Param("params") Parmas params);

    @Select("select * from bill where  bookname = #{bookname}  limit 1")
    IncomeBill findByName(@Param("bookname")String bookname);

    @Select("select * from bill where inoutclassify = '收入' and  uname = #{params.uname} ")
    List<IncomeBill> getIncomeBill(String uname);

    @Select("SELECT *  FROM bill WHERE inoutclassify = '收入' and  uname = #{params.uname}")
    List<IncomeBill> findAll(@Param("params") Parmas params);

}