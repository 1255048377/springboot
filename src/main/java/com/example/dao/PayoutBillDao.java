package com.example.dao;

import com.example.entity.IncomeBill;
import com.example.entity.Parmas;
import com.example.entity.PayoutBill;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;


@Repository
public interface PayoutBillDao extends Mapper<PayoutBill> {


    @Select("select * from bill where inoutclassify = '支出' and bookname like concat('%', #{params.bookname}, '%') and billclassify like concat('%', #{params.billclassify}, '%') and uname =#{params.uname} ")
    List<PayoutBill> findBySearch(@Param("params") Parmas params);

    @Select("select * from bill where  bookname = #{bookname}  limit 1")
    IncomeBill findByName(@Param("bookname")String bookname);

    @Select("select * from bill where inoutclassify = '支出'")
    List<PayoutBill> getPayoutBill();

    @Select("SELECT *  FROM bill WHERE inoutclassify = '支出' and uname = #{params.uname}")
    List<PayoutBill> findAll(@Param("params") Parmas params);

    @Select("SELECT *  FROM bill where uname = #{params.uname}")
    List<PayoutBill> findAllinout(@Param("params") Parmas params);
}